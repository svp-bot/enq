package main

import (
	"context"
	"crypto/tls"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/google/subcommands"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"git.autistici.org/ai3/tools/enq/engine"
	pb "git.autistici.org/ai3/tools/enq/proto"
)

type serverCommand struct {
	addr      string
	httpAddr  string
	dbPath    string
	shard     string
	topology  string
	authToken string
	sslCert   string
	sslKey    string
	sslCA     string
}

func (c *serverCommand) Name() string     { return "server" }
func (c *serverCommand) Synopsis() string { return "run the queue engine" }
func (c *serverCommand) Usage() string {
	return `server:
        Run the queue engine server.

`
}

func (c *serverCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.shard, "shard", "", "shard identifier for this node")
	f.StringVar(&c.addr, "addr", ":3733", "address to listen on for the GRPC API")
	f.StringVar(&c.httpAddr, "http-addr", "", "address to listen on for the HTTP API (optional)")
	f.StringVar(&c.dbPath, "db", "/var/lib/enq/queue.db", "`path` of the database file")
	f.StringVar(&c.topology, "topology", "", "service topology specification for peer discovery, TYPE=SPEC, where TYPE can be 'static' (SPEC is a comma-separated list of shard=addr assignments) or 'dynamic' (SPEC is a pattern containing a literal '%s' token that will be replaced by the shard ID)")
	f.StringVar(&c.authToken, "auth-token", "", "shared authentication secret")
	f.StringVar(&c.sslCert, "ssl-cert", "", "SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", "", "SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", "", "SSL CA `path`")
}

func (c *serverCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}
	if c.shard == "" {
		log.Printf("error: must specify --shard")
		return subcommands.ExitUsageError
	}

	var topo engine.Topology
	if c.topology != "" {
		var err error
		topo, err = parseTopology(c.topology)
		if err != nil {
			log.Printf("error: bad --topology: %v", err)
			return subcommands.ExitUsageError
		}
	}

	if err := c.runServer(ctx, topo); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}

	return subcommands.ExitSuccess
}

func (c *serverCommand) runServer(ctx context.Context, topo engine.Topology) error {
	// Initialize the database and the queue engine.
	db, err := engine.OpenDB(c.dbPath)
	if err != nil {
		return fmt.Errorf("error opening database: %w", err)
	}
	defer db.Close()

	dialer := newGRPCDialer(c.authToken, c.sslCert, c.sslKey, c.sslCA)
	e := engine.New(db, c.shard, topo, dialer)
	defer e.Close()

	// Create a controlling context that can be canceled to
	// shutdown, and an errgroup below that to run the servers.
	sctx, cancel := context.WithCancel(ctx)
	g, ictx := errgroup.WithContext(sctx)

	// Install a termination signal handler.
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("received termination signal, exiting")
		cancel()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	// Create a common TLS configuration to be shared between the
	// HTTP and GRPC servers.
	var tlsconf *tls.Config
	if c.sslCert != "" && c.sslKey != "" && c.sslCA != "" {
		tlsconf, err = serverTLSConfig(c.sslCert, c.sslKey, c.sslCA)
		if err != nil {
			return err
		}
	}

	// Optionally initialize the HTTP server.
	if c.httpAddr != "" {
		g.Go(func() error {
			// Create an outer ServeMux to handle the default
			// debug pages too. Only forward /debug/task* to the
			// engine's debug pages.
			mux := http.NewServeMux()
			mux.Handle("/debug/task", e)
			mux.Handle("/debug/tasks", e)
			mux.Handle("/debug/", http.DefaultServeMux)
			mux.Handle("/metrics", promhttp.Handler())

			server := &http.Server{
				Addr:         c.httpAddr,
				TLSConfig:    tlsconf,
				Handler:      mux,
				ReadTimeout:  10 * time.Second,
				IdleTimeout:  30 * time.Second,
				WriteTimeout: 10 * time.Second,
			}

			return runHTTPServerWithContext(ctx, server)
		})
	}

	// Initialize the GRPC server.
	g.Go(func() error {
		var opts = []grpc.ServerOption{
			grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
				grpc_prometheus.UnaryServerInterceptor,
				engine.SqliteServerInterceptor(),
			)),
			grpc.StreamInterceptor(
				grpc_prometheus.StreamServerInterceptor,
			),
		}
		if c.authToken != "" {
			opts = append(opts, grpc.UnaryInterceptor(
				authTokenInterceptor(c.authToken)))
		}
		if tlsconf != nil {
			opts = append(opts, grpc.Creds(credentials.NewTLS(tlsconf)))
		}

		server := grpc.NewServer(opts...)
		pb.RegisterInternalServer(server, e)
		pb.RegisterWorkerServer(server, e)
		pb.RegisterQueueServer(server, e)
		grpc_prometheus.Register(server)

		return runGRPCServerWithContext(ictx, server, c.addr)
	})

	return g.Wait()
}

func runGRPCServerWithContext(ctx context.Context, server *grpc.Server, addr string) error {
	l, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	defer l.Close()

	go func() {
		<-ctx.Done()
		server.GracefulStop()
	}()

	return server.Serve(l)
}

func runHTTPServerWithContext(ctx context.Context, server *http.Server) error {
	go func() {
		<-ctx.Done()
		sctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := server.Shutdown(sctx); err != nil {
			server.Close() // nolint: errcheck
		}
	}()

	return server.ListenAndServe()
}

func parseTopology(s string) (engine.Topology, error) {
	switch {
	case strings.HasPrefix(s, "static:"):
		peers := make(map[string]string)
		for _, token := range strings.Split(s[7:], ",") {
			parts := strings.SplitN(token, "=", 2)
			if len(parts) != 2 {
				return nil, errors.New("bad syntax")
			}
			peers[parts[0]] = parts[1]
		}
		return engine.NewStaticTopology(peers), nil

	case strings.HasPrefix(s, "dynamic:"):
		pattern := s[8:]
		if !strings.Contains(pattern, "%s") {
			return nil, errors.New("dynamic pattern must contain a '%s' literal token")
		}
		return engine.NewShardedTopology(pattern), nil

	default:
		return nil, errors.New("invalid type")
	}
}

func init() {
	subcommands.Register(&serverCommand{}, "")
}

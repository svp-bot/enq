package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/google/subcommands"

	"git.autistici.org/ai3/tools/enq/worker"
)

type workerCommand struct {
	serverAddr string
	scriptDir  string
	id         string
	authToken  string
	sslCert    string
	sslKey     string
	sslCA      string
	numWorkers int
}

func (c *workerCommand) Name() string     { return "worker" }
func (c *workerCommand) Synopsis() string { return "run the worker" }
func (c *workerCommand) Usage() string {
	return `worker:
        Run the queue engine worker.

`
}

func (c *workerCommand) SetFlags(f *flag.FlagSet) {
	f.StringVar(&c.serverAddr, "server", "127.0.0.1:3733", "address of the local queue engine server")
	f.StringVar(&c.scriptDir, "dir", "/etc/enq/worker.d", "directory containing task handlers")
	f.StringVar(&c.id, "id", "", "unique worker identifier")
	f.StringVar(&c.authToken, "auth-token", "", "shared authentication secret")
	f.StringVar(&c.sslCert, "ssl-cert", "", "SSL certificate `path`")
	f.StringVar(&c.sslKey, "ssl-key", "", "SSL private key `path`")
	f.StringVar(&c.sslCA, "ssl-ca", "", "SSL CA `path`")
	f.IntVar(&c.numWorkers, "workers", 5, "worker concurrency")
}

func (c *workerCommand) Execute(ctx context.Context, f *flag.FlagSet, args ...interface{}) subcommands.ExitStatus {
	if f.NArg() > 0 {
		log.Printf("error: too many arguments")
		return subcommands.ExitUsageError
	}
	if c.id == "" {
		log.Printf("error: must specify --id")
		return subcommands.ExitUsageError
	}

	if err := c.runWorker(ctx); err != nil {
		log.Printf("error: %v", err)
		return subcommands.ExitFailure
	}
	return subcommands.ExitSuccess
}

func (c *workerCommand) runWorker(ctx context.Context) error {
	// Initialize the GRPC connection.
	dialer := newGRPCDialer(c.authToken, c.sslCert, c.sslKey, c.sslCA)
	conn, err := dialer(ctx, c.serverAddr)
	if err != nil {
		return err
	}
	defer conn.Close()

	// Start the workers. No base URL for now as there is no
	// debug HTTP server yet.
	log.Printf("starting %d concurrent workers", c.numWorkers)
	mgr := worker.NewManager(conn, c.id, "", c.scriptDir, c.numWorkers)

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("received termination signal, exiting")
		mgr.Stop()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	// The main thread has nothing to do except waiting on a
	// termination signal.
	mgr.Wait()
	log.Printf("stopped workers")

	return nil
}

func init() {
	subcommands.Register(&workerCommand{}, "")
}

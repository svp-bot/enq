package engine

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"

	pb "git.autistici.org/ai3/tools/enq/proto"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func TestDebug(t *testing.T) {
	n := newTestNet()
	defer n.Close()
	for i := 0; i < 5; i++ {
		n.newServer(t, fmt.Sprintf("%d", i+1))
	}
	n.startWorkers(t)

	deadline := time.Now().AddDate(1, 0, 0)
	future := time.Now().AddDate(0, 0, 1)

	c, _ := n.net.GetQueue(context.Background(), "1")
	resp, err := c.Submit(
		context.Background(),
		&pb.SubmitRequest{
			Tasks: []*pb.TaskSpec{
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "2",
						Id:    "aaa",
					},
					Method:    "fail",
					NotBefore: timestamppb.New(future),
					Deadline:  timestamppb.New(deadline),
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "3",
						Id:    "bbb",
					},
					Method: "neg",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("42"),
						},
					},
					// Schedule this one sometime
					// in the future so it has a
					// chance to be canceled.
					NotBefore: timestamppb.New(future),
					Deadline:  timestamppb.New(deadline),
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "4",
						Id:    "ccc",
					},
					Method: "sum",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type:      pb.ArgumentSpec_VALUE,
							ValueJson: []byte("19"),
						},
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "3",
								Id:    "bbb",
							},
						},
					},
					NotBefore: timestamppb.New(future),
					Deadline:  timestamppb.New(deadline),
				},
				&pb.TaskSpec{
					TaskId: &pb.ShardedID{
						Shard: "5",
						Id:    "ddd",
					},
					Method: "sum",
					Args: []*pb.ArgumentSpec{
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "4",
								Id:    "ccc",
							},
						},
						&pb.ArgumentSpec{
							Type: pb.ArgumentSpec_TASKREF,
							TaskId: &pb.ShardedID{
								Shard: "2",
								Id:    "aaa",
							},
						},
					},
					NotBefore: timestamppb.New(future),
					Deadline:  timestamppb.New(deadline),
				},
			},
		},
	)

	if err != nil {
		t.Fatal(err)
	}

	// Start an HTTP server on a random interface.
	svc := n.servers[0].svc
	httpSrv := httptest.NewServer(svc)
	defer httpSrv.Close()
	httpCli := new(http.Client)

	httpResp, err := httpCli.Get(httpSrv.URL + "/debug/task?t=" + url.QueryEscape(resp.RootTaskId.ToString()))
	if err != nil {
		t.Fatalf("HTTP /debug/task error: %v", err)
	}
	httpResp.Body.Close()
	if httpResp.StatusCode != 200 {
		t.Fatalf("HTTP /debug/task bad status code: %d", httpResp.StatusCode)
	}

	httpResp, err = httpCli.Get(httpSrv.URL + "/debug/tasks")
	if err != nil {
		t.Fatalf("HTTP /debug/tasks error: %v", err)
	}
	httpResp.Body.Close()
	if httpResp.StatusCode != 200 {
		t.Fatalf("HTTP /debug/tasks bad status code: %d", httpResp.StatusCode)
	}
}

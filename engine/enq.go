package engine

import (
	"context"
	"database/sql"
	"io"
	"log"
	"sync"
	"time"

	pb "git.autistici.org/ai3/tools/enq/proto"
	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
)

var (
	// Delay for retrying a task after a temporary error.
	errorRetryDelay = 300 * time.Second

	// Lease timeout.
	leaseTimeout = 300 * time.Second

	// How often to check for pending transactions (retry failed
	// ones etc).
	pendingTxCheckInterval = 10 * time.Second

	// How ofter to check for expired leases.
	leaseCheckInterval = 10 * time.Second

	// Timeout for task state processing operations.
	tstProcessTimeout = 30 * time.Second
)

// Generic unique identifier for transient TaskStateTransition records.
type rowID int64

// Global task statistics.
type stats struct {
	NumRunning     int
	NumSchedulable int
	NumPending     int
	NumDone        int
}

// Database transaction interface.
type databaseTx interface {
	GetTaskSpec(*pb.ShardedID) (*pb.TaskSpec, error)
	GetTaskState(*pb.ShardedID) (pb.TaskInfo_State, error)

	AddTask(*pb.TaskSpec) error
	SetTaskState(*pb.ShardedID, pb.TaskInfo_State, time.Time) error
	SetTaskResultID(*pb.ShardedID, *pb.ShardedID) error
	StoreTaskResult(*pb.ShardedID, []byte) (*pb.ShardedID, error)
	GetTaskResultByID(*pb.ShardedID) ([]byte, error)
	GetTaskResultByTaskID(*pb.ShardedID) ([]byte, error)
	IsSchedulable(*pb.ShardedID) bool

	NextLease(string, string) (*pb.ShardedID, error)
	RenewLease(*pb.ShardedID) error
	GetExpiredLeases() ([]*pb.ShardedID, error)

	PushTaskStateTransition(*pb.TaskStateTransition) (rowID, error)
	PopTaskStateTransition(rowID) (*pb.TaskStateTransition, error)
	OnPendingTaskStateTransitions(func(rowID) error) error

	AddLog(*pb.ShardedID, []byte) error
	GetLogs(*pb.ShardedID) (io.Reader, error)

	// Debugging methods.
	GetStats() (*stats, error)
	GetPendingTasks(int) ([]*pb.ShardedID, error)
}

// The database interface is just a transaction factory.
type database interface {
	WithTx(context.Context, func(databaseTx) error) error
}

// Engine implements a queue engine for a specific shard.
type Engine struct {
	db    database
	net   *network
	shard string

	stopCh   chan struct{}
	notifyCh chan rowID

	pb.UnimplementedInternalServer
	pb.UnimplementedWorkerServer
	pb.UnimplementedQueueServer
}

func newEngine(db database, shard string, net *network) *Engine {
	s := &Engine{
		db:       db,
		net:      net,
		shard:    shard,
		notifyCh: make(chan rowID, 100),
		stopCh:   make(chan struct{}),
	}
	go s.stateProc()
	go s.stateLoop()
	go s.leaseLoop()
	return s
}

// New creates a new Engine with the specified parameters.
//
// You're supposed to provide it with an open SQL database handler,
// and a set of network-related parameters to tell it how to find the
// other peer nodes in the service.
func New(db *sql.DB, shard string, topology Topology, dialer Dialer) *Engine {
	return newEngine(newSQLDB(shard, db), shard, newNetwork(topology, dialer))
}

// Close the engine and all associated resources.
func (s *Engine) Close() {
	close(s.stopCh)
}

type runningTask struct {
	spec  *pb.TaskSpec
	state pb.TaskInfo_State
	down  *pb.ShardedID
}

// Fetch all the data about a task that we need in order to handle a
// state transition. Used to keep handleTaskStateTransition() slightly
// shorter.
func getTask(tx databaseTx, id *pb.ShardedID) (*runningTask, error) {
	state, err := tx.GetTaskState(id)
	if err != nil {
		return nil, err
	}
	spec, err := tx.GetTaskSpec(id)
	if err != nil {
		return nil, err
	}
	return &runningTask{
		state: state,
		spec:  spec,
		down:  spec.DownstreamTaskId,
	}, nil
}

// Handle a state transition for a specific task, expressed as a
// TaskStateTransition object. Will make RPC calls to other shards in
// order to propagate state.
func (s *Engine) handleTaskStateTransition(ctx context.Context, tx databaseTx, ts *pb.TaskStateTransition) error {
	task, err := getTask(tx, ts.TaskId)
	if err != nil {
		return err
	}

	if task.state.IsFinal() {
		// Ignore this transition, it was probably received
		// late. Throw away the result.
		return nil
	}

	log.Printf("tst: task=%s, state=%s, result=%s", task.spec.TaskId.ToString(), task.state, ts.ResultStatus)

	// TEMPORARY_FAILURE. If the task hasn't reached its deadline,
	// schedule it again for a retry some time in the
	// future. Otherwise turn the status into a permanent FAILURE.
	if ts.ResultStatus == pb.TaskStateTransition_TEMPORARY_FAILURE {
		if task.spec.Deadline.AsTime().Before(time.Now()) {
			ts.ResultStatus = pb.TaskStateTransition_FAILURE
		} else {
			when := time.Now().Add(errorRetryDelay)
			return tx.SetTaskState(ts.TaskId, pb.TaskInfo_SCHEDULABLE, when)
		}
	}

	// FAILURE. Set the downstream task status to FAILED, and have
	// it cancel all of its *upstream* dependencies (except for
	// the current task) recursively.
	if ts.ResultStatus == pb.TaskStateTransition_FAILURE {
		return s.handleTaskStateTransitionFailure(ctx, tx, ts, task)
	}

	// SUCCESS. Report success to the downstream task, and store
	// the result on that shard.
	return s.handleTaskStateTransitionSuccess(ctx, tx, ts, task)
}

func (s *Engine) handleTaskStateTransitionFailure(ctx context.Context, tx databaseTx, ts *pb.TaskStateTransition, task *runningTask) error {
	if err := tx.SetTaskState(ts.TaskId, pb.TaskInfo_FAILED, time.Time{}); err != nil {
		return err
	}

	if task.down != nil {
		req := &pb.SetFailedRequest{
			TaskId:   task.down,
			ParentId: ts.TaskId,
		}
		var err error

		// Short-circuit the request to the local shard keeping
		// the current database transaction.
		if s.shard == task.down.Shard {
			err = s.internalSetFailed(ctx, tx, req)
		} else {
			peer, perr := s.net.GetPeer(ctx, task.down.Shard)
			if perr != nil {
				return perr
			}
			_, err = peer.SetFailed(ctx, req, grpc_retry.WithMax(3))
		}
		if err != nil {
			return err
		}
	}

	// Ok, done.
	return nil
}

func (s *Engine) handleTaskStateTransitionSuccess(ctx context.Context, tx databaseTx, ts *pb.TaskStateTransition, task *runningTask) error {
	if err := tx.SetTaskState(ts.TaskId, pb.TaskInfo_SUCCESS, time.Time{}); err != nil {
		return err
	}

	var resultID *pb.ShardedID
	if task.down != nil {
		// Send result downstream.
		req := &pb.ReportSuccessRequest{
			TaskId:           ts.TaskId,
			DownstreamTaskId: task.down,
			ResultData:       ts.ResultData,
		}
		var resp *pb.ReportSuccessResponse
		var err error

		// Short-circuit the request to the local shard keeping the
		// current database transaction.
		if s.shard == task.down.Shard {
			resp, err = s.internalReportSuccess(ctx, tx, req)
		} else {
			peer, perr := s.net.GetPeer(ctx, task.down.Shard)
			if perr != nil {
				return perr
			}
			resp, err = peer.ReportSuccess(ctx, req, grpc_retry.WithMax(3))
		}
		if err != nil {
			return err
		}
		resultID = resp.ResultId
	} else {
		// Store result locally? It has no consumers so it
		// will never be used.
		log.Printf("task %s had no downstream consumers, storing result locally", ts.TaskId.ToString())
		var err error
		resultID, err = tx.StoreTaskResult(ts.TaskId, ts.ResultData)
		if err != nil {
			return err
		}
	}

	// Store the association between task -> result, for debugging
	// purposes. The result is only used on the downstream shard.
	return tx.SetTaskResultID(ts.TaskId, resultID)
}

func (s *Engine) processTransition(ctx context.Context, tid rowID) error {
	return s.db.WithTx(ctx, func(tx databaseTx) error {
		tst, err := tx.PopTaskStateTransition(tid)
		if err != nil {
			return err
		}
		return s.handleTaskStateTransition(ctx, tx, tst)
	})
}

func (s *Engine) stateProc() {
	var wg sync.WaitGroup
	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				select {
				case rowID := <-s.notifyCh:
					ctx, cancel := context.WithTimeout(context.Background(), tstProcessTimeout)
					err := s.processTransition(ctx, rowID)
					cancel()
					if err != nil {
						log.Printf("error processing task state transition %d: %v", rowID, err)
					}

				case <-s.stopCh:
					return
				}
			}
		}()
	}

	wg.Wait()
}

// Process all the pending TaskStateTransitions.
func (s *Engine) processPendingTransitions(ctx context.Context) {
	// nolint: errcheck
	s.db.WithTx(ctx, func(tx databaseTx) error {
		return tx.OnPendingTaskStateTransitions(func(tid rowID) error {
			s.notifyCh <- tid
			return nil
		})
	})
}

func (s *Engine) stateLoop() {
	ticker := time.NewTicker(pendingTxCheckInterval)
	defer ticker.Stop()
	for {
		select {
		case <-ticker.C:
			s.processPendingTransitions(context.Background())
		case <-s.stopCh:
			return
		}
	}
}

func (s *Engine) expireLeases(ctx context.Context) {
	if err := s.db.WithTx(ctx, func(tx databaseTx) error {
		when := time.Now().Add(errorRetryDelay)
		expired, err := tx.GetExpiredLeases()
		if err != nil {
			return err
		}
		for _, id := range expired {
			if err := tx.SetTaskState(id, pb.TaskInfo_SCHEDULABLE, when); err != nil {
				log.Printf("error rescheduling task %s: %v", id.ToString(), err)
			}
		}
		return nil
	}); err != nil {
		// Just print a warning, no other action possible.
		log.Printf("error fetching expired leases: %v", err)
	}
}

func (s *Engine) leaseLoop() {
	ticker := time.NewTicker(leaseCheckInterval)
	defer ticker.Stop()
	for {
		select {
		case <-s.stopCh:
			return
		case <-ticker.C:
			s.expireLeases(context.Background())
		}
	}
}

package engine

import (
	"context"
	"errors"
	"strings"
	"sync"

	pb "git.autistici.org/ai3/tools/enq/proto"
	"golang.org/x/sync/singleflight"
	"google.golang.org/grpc"
)

// Topology provides the network addresses corresponding to specific
// shards of the service.
type Topology interface {
	GetShardAddr(string) (string, error)
}

type staticTopology struct {
	peers map[string]string
}

func (t *staticTopology) GetShardAddr(shard string) (string, error) {
	addr, ok := t.peers[shard]
	if !ok {
		return "", errors.New("unknown shard")
	}
	return addr, nil
}

// NewStaticTopology returns a Topology with a statically-defined
// assignment of shards to addresses.
func NewStaticTopology(peers map[string]string) Topology {
	return &staticTopology{peers: peers}
}

type shardedTopology struct {
	pattern string
}

// NewShardedTopology returns a Topology that dynamically builds
// network addresses based on a pattern. The pattern, in host:port
// format, must contain a literal '%s' token, that will be replaced by
// the shard ID to obtain the full network address.
func NewShardedTopology(pattern string) Topology {
	return &shardedTopology{pattern: pattern}
}

func (t *shardedTopology) GetShardAddr(shard string) (string, error) {
	return strings.Replace(t.pattern, "%s", shard, -1), nil
}

// Dialer dials a network address. Override to set specific GRPC
// options. The default implementation just calls grpc.DialContext.
type Dialer func(context.Context, string) (*grpc.ClientConn, error)

func defaultDialer(ctx context.Context, addr string) (*grpc.ClientConn, error) {
	return grpc.DialContext(ctx, addr)
}

// The network object combines a Dialer and a Topology with a cache of
// per-shard GRPC connections. A singleflight group is used to avoid
// holding the cache lock for too long when establishing a slow
// connection. Connections are cached for the lifetime of the object.
type network struct {
	dialer   Dialer
	topology Topology

	mx        sync.Mutex
	sg        singleflight.Group
	connCache map[string]*grpc.ClientConn
}

func newNetwork(topology Topology, dialer Dialer) *network {
	if dialer == nil {
		dialer = defaultDialer
	}
	return &network{
		connCache: make(map[string]*grpc.ClientConn),
		dialer:    dialer,
		topology:  topology,
	}
}

func (n *network) connect(ctx context.Context, shard string) (*grpc.ClientConn, error) {
	n.mx.Lock()
	conn, ok := n.connCache[shard]
	n.mx.Unlock()

	if !ok {
		value, err, _ := n.sg.Do(shard, func() (interface{}, error) {
			addr, err := n.topology.GetShardAddr(shard)
			if err != nil {
				return nil, err
			}
			conn, err := n.dialer(ctx, addr)
			if err != nil {
				return nil, err
			}

			n.mx.Lock()
			n.connCache[shard] = conn
			n.mx.Unlock()

			return conn, nil
		})
		if err != nil {
			return nil, err
		}
		conn = value.(*grpc.ClientConn)
	}

	return conn, nil
}

func (n *network) GetPeer(ctx context.Context, shard string) (pb.InternalClient, error) {
	conn, err := n.connect(ctx, shard)
	if err != nil {
		return nil, err
	}
	return pb.NewInternalClient(conn), nil
}

func (n *network) GetQueue(ctx context.Context, shard string) (pb.QueueClient, error) {
	conn, err := n.connect(ctx, shard)
	if err != nil {
		return nil, err
	}
	return pb.NewQueueClient(conn), nil
}

// Only used in tests.
func (n *network) getWorker(ctx context.Context, shard string) (pb.WorkerClient, error) {
	conn, err := n.connect(ctx, shard)
	if err != nil {
		return nil, err
	}
	return pb.NewWorkerClient(conn), nil
}

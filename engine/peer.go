package engine

import (
	"context"
	"time"

	pb "git.autistici.org/ai3/tools/enq/proto"
	"github.com/golang/protobuf/ptypes/empty"
	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
)

var emptyResponse = new(empty.Empty)

// All of the following RPC methods, which are called by the task state
// transition handler while holding a database transaction, have an "internal"
// implementation that can be called providing the outer db transaction. RPC
// calls to the same shard done by the handler are then short-circuited to these
// methods instead of going through the GRPC layer. This prevents transaction
// deadlocking on backends such as SQLite.

// Cancel all upstreams recursively. Every SetCancel call returns a
// list of further upstreams.
func (s *Engine) cancelUpstreams(ctx context.Context, tx databaseTx, avoid *pb.ShardedID, upstreams []*pb.ShardedID) error {
	for _, up := range upstreams {
		if up.Equal(avoid) {
			continue
		}

		var resp *pb.SetCancelResponse
		var err error
		req := &pb.SetCancelRequest{
			TaskId: up,
		}

		// Short-circuit the request to the local shard keeping the
		// current database transaction.
		if s.shard == up.Shard {
			resp, err = s.internalSetCancel(ctx, tx, req)
		} else {
			peer, perr := s.net.GetPeer(ctx, up.Shard)
			if perr != nil {
				return perr
			}
			resp, err = peer.SetCancel(ctx, req, grpc_retry.WithMax(3))
		}
		if err != nil {
			return err
		}

		if len(resp.UpstreamTaskIds) > 0 {
			if err := s.cancelUpstreams(ctx, tx, avoid, resp.UpstreamTaskIds); err != nil {
				return err
			}
		}
	}
	return nil
}

func (s *Engine) internalSetFailed(ctx context.Context, tx databaseTx, req *pb.SetFailedRequest) error {
	if err := tx.SetTaskState(req.TaskId, pb.TaskInfo_FAILED, time.Time{}); err != nil {
		return err
	}

	spec, err := tx.GetTaskSpec(req.TaskId)
	if err != nil {
		return err
	}
	return s.cancelUpstreams(ctx, tx, req.ParentId, spec.GetTaskArgIDs())
}

// SetFailed is received when an upstream task has failed. Set the
// task state to FAILED, and cancel all of its upstream dependencies.
//
// The recursive tree navigation is performed locally on this shard,
// to avoid building a complex network of nested RPCs.
func (s *Engine) SetFailed(ctx context.Context, req *pb.SetFailedRequest) (*empty.Empty, error) {
	err := s.db.WithTx(ctx, func(tx databaseTx) error {
		return s.internalSetFailed(ctx, tx, req)
	})
	return emptyResponse, err
}

func (s *Engine) internalSetCancel(ctx context.Context, tx databaseTx, req *pb.SetCancelRequest) (*pb.SetCancelResponse, error) {
	var resp pb.SetCancelResponse

	spec, err := tx.GetTaskSpec(req.TaskId)
	if err != nil {
		return nil, err
	}
	resp.UpstreamTaskIds = spec.GetTaskArgIDs()

	return &resp, tx.SetTaskState(req.TaskId, pb.TaskInfo_CANCELED, time.Time{})
}

// SetCancel is received when a downstream task has failed. This
// method is called recursively from the failed task's shard, so in
// order to avoid an extra GetTaskSpec RPC we directly return the list
// of upstream tasks to the caller.
func (s *Engine) SetCancel(ctx context.Context, req *pb.SetCancelRequest) (*pb.SetCancelResponse, error) {
	// Return the upstreams, so that the caller can cancel them
	// too, recursing up the tree.
	var resp *pb.SetCancelResponse
	err := s.db.WithTx(ctx, func(tx databaseTx) (err error) {
		resp, err = s.internalSetCancel(ctx, tx, req)
		return
	})
	return resp, err
}

func (s *Engine) internalReportSuccess(ctx context.Context, tx databaseTx, req *pb.ReportSuccessRequest) (*pb.ReportSuccessResponse, error) {
	var resp pb.ReportSuccessResponse

	resultID, err := tx.StoreTaskResult(req.TaskId, req.ResultData)
	if err != nil {
		return nil, err
	}
	resp.ResultId = resultID

	if tx.IsSchedulable(req.DownstreamTaskId) {
		// Got to fetch the spec in order to find the not_before field.
		spec, serr := tx.GetTaskSpec(req.DownstreamTaskId)
		if serr != nil {
			return nil, serr
		}
		err = tx.SetTaskState(req.DownstreamTaskId, pb.TaskInfo_SCHEDULABLE, spec.NotBefore.AsTime())
	}

	return &resp, err
}

// ReportSuccess is received when an upstream task has succeeded. We
// will store its result locally, so that it is available to the
// downstream task on this same shard. If the downstream task has no
// other dependencies, it will be marked as schedulable.
func (s *Engine) ReportSuccess(ctx context.Context, req *pb.ReportSuccessRequest) (*pb.ReportSuccessResponse, error) {
	var resp *pb.ReportSuccessResponse
	err := s.db.WithTx(ctx, func(tx databaseTx) (err error) {
		resp, err = s.internalReportSuccess(ctx, tx, req)
		return
	})
	return resp, err
}

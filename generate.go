//go:generate go-bindata --nocompress --pkg migrations --ignore \.go$ -o migrations/bindata.go -prefix migrations/ ./migrations
//go:generate protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative proto/enq.proto
package enq

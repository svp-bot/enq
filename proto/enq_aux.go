package proto

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *ShardedID) Valid() error {
	if s.Shard == "" {
		return errors.New("empty shard")
	}
	if s.Id == "" {
		return errors.New("empty id")
	}
	return nil
}

func (s *ShardedID) Equal(other *ShardedID) bool {
	return s.Shard == other.Shard && s.Id == other.Id
}

func (s *ShardedID) ToString() string {
	if s == nil {
		return ""
	}
	return fmt.Sprintf("%s/%s", s.Shard, s.Id)
}

var errBadIDFmt = errors.New("bad ID format")

func ParseShardedID(s string) (*ShardedID, error) {
	parts := strings.SplitN(s, "/", 2)
	if len(parts) < 2 {
		return nil, errBadIDFmt
	}
	if parts[0] == "" || parts[1] == "" {
		return nil, errBadIDFmt
	}
	return &ShardedID{
		Shard: parts[0],
		Id:    parts[1],
	}, nil
}

func (i TaskInfo_State) IsFinal() bool {
	switch i {
	case TaskInfo_CANCELED, TaskInfo_FAILED, TaskInfo_SUCCESS:
		return true
	default:
		return false
	}
}

func (s *TaskSpec) GetTaskArgs() []*ArgumentSpec {
	var out []*ArgumentSpec
	for _, arg := range s.Args {
		if arg.Type == ArgumentSpec_TASKREF {
			out = append(out, arg)
		}
	}
	return out
}

func (s *TaskSpec) GetTaskArgIDs() []*ShardedID {
	var out []*ShardedID
	for _, arg := range s.Args {
		if arg.Type == ArgumentSpec_TASKREF {
			out = append(out, arg.TaskId)
		}
	}
	return out
}

// Valid performs sanity checks on ArgumentSpec.
func (a *ArgumentSpec) Valid() error {
	switch a.Type {
	case ArgumentSpec_VALUE:
		if len(a.ValueJson) == 0 {
			return errors.New("empty value")
		}
	case ArgumentSpec_TASKREF:
		if a.TaskId == nil {
			return errors.New("empty task_id")
		}
		if err := a.TaskId.Valid(); err != nil {
			return err
		}
	default:
		return errors.New("invalid argument type")
	}
	return nil
}

// Valid performs sanity checks on TaskSpec.
func (s *TaskSpec) Valid() error {
	if s.TaskId == nil {
		return errors.New("empty task_id")
	}
	if err := s.TaskId.Valid(); err != nil {
		return err
	}

	if s.Method == "" {
		return errors.New("empty method")
	}

	for _, arg := range s.Args {
		if err := arg.Valid(); err != nil {
			return err
		}
	}
	return nil
}

// SetDefaults sets defaults for unspcified fields.
func (s *TaskSpec) SetDefaults() {
	if s.NotBefore == nil {
		s.NotBefore = timestamppb.New(time.Now())
	}
	// Default deadline is (very arbitrarily) 1 year from now.
	if s.Deadline == nil {
		s.Deadline = timestamppb.New(time.Now().AddDate(1, 0, 0))
	}
}

package worker

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	pb "git.autistici.org/ai3/tools/enq/proto"
)

const tempErrorExitCode = 42

type tempError error

type taskHandler interface {
	HandleTask(context.Context, *pb.Task, *log.Logger) ([]byte, error)
}

// Process-based task handler. For each task, it runs a script from
// its "script directory" named after the task method, passing the
// arguments on stdin as a JSON-encoded list of objects, and expecting
// the result as a JSON-encoded object on stdout.
type procTaskHandler struct {
	dir string
}

// Build the JSON argument list. Args are already encoded so we just
// quickly build a list of them. Don't really care much about errors
// here.
//
// nolint: errcheck
func (t *procTaskHandler) buildArgsJSON(args [][]byte) io.Reader {
	var buf bytes.Buffer

	io.WriteString(&buf, "[")
	for i, arg := range args {
		if i > 0 {
			io.WriteString(&buf, ",")
		}
		buf.Write(arg)
	}
	io.WriteString(&buf, "]")
	return &buf
}

func (t *procTaskHandler) HandleTask(ctx context.Context, task *pb.Task, l *log.Logger) ([]byte, error) {
	// Find the script to handle this task.
	script := filepath.Join(t.dir, task.Spec.Method)

	// Check early for existence.
	if _, err := os.Stat(script); os.IsNotExist(err) {
		return nil, fmt.Errorf("no handler available for method '%s'", task.Spec.Method)
	}

	// Execute the script.
	log.Printf("handler: executing %s", script)
	cmd := exec.CommandContext(ctx, script)
	cmd.Stdin = t.buildArgsJSON(task.Args)

	stderr, err := cmd.StderrPipe()
	if err != nil {
		return nil, tempError(err)
	}
	go lineLogger(stderr, l)

	output, err := cmd.Output()

	// Handle errors.
	if err != nil {
		var exit *exec.ExitError
		if errors.As(err, &exit) {
			if exit.ExitCode() == tempErrorExitCode {
				return nil, tempError(err)
			}
		}
		return nil, err
	}

	return output, nil
}

func lineLogger(stderr io.Reader, l *log.Logger) {
	scanner := bufio.NewScanner(stderr)
	for scanner.Scan() {
		l.Print(scanner.Bytes())
	}
}
